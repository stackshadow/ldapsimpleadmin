#!/bin/python

import sys
import ldap
import ldap.modlist
import subprocess

class ldapAdmin:
	message = ''
	ldapHost = ''
	ldapUser = ''
	ldapPass = ''
	ldapTempModifyFile = '/tmp/ldapmodify.ldiff'
	ldapTempLogFile = '/tmp/ldapmodify.log'

	def __init__( self ):
		self.connected = False
		self.errorMessage = ''
		print 'init ldapAdmin...'

	def setCredentials( self, host, username, password ):
		self.ldapHost = host
		self.ldapUser = username
		self.ldapPass = password
        
	def encryptPassword( self, password ):
		systemCommand = 'slappasswd -h {SSHA} -s %s' % password
		encryptPassword = ''

		try:
			out_bytes = subprocess.check_output( systemCommand, shell=True )
			out_text = out_bytes.decode('utf-8')
			encryptPassword = out_text.replace("\n", "")

		except subprocess.CalledProcessError:
			self.message = 'Error command not exist: %s' % systemCommand
		except:
			self.message = 'Error: %s' % sys.exc_info()[0]
			
		return encryptPassword

# get infos
	def posixUserList( self, userBaseDN ):
		userList = []
		systemCommand = 'ldapsearch -H %s -D "%s" -w "%s" -LLL -b %s | grep dn:' % ( self.ldapHost, self.ldapUser, self.ldapPass, userBaseDN )

		try:
			out_bytes = subprocess.check_output( systemCommand, shell=True )
			out_text = out_bytes.decode('utf-8')


			lines = out_text.split( "\n" )
			for line in lines:
				values = line.split( " " )
				if( len(values) > 1 ):
					userList.append( values[1] )
				
			self.message = 'List of users ok'

		except subprocess.CalledProcessError:
			self.message = 'Error command not exist: %s' % systemCommand
		except:
			self.message = 'Error: %s' % sys.exc_info()[0]
			
		return userList

	def posixUserLoad( self, userDN ):
		attributeList = {}
		systemCommand = 'ldapsearch -H %s -D "%s" -w "%s" -LLL -b %s @inetOrgPerson @posixAccount memberOf' % ( self.ldapHost, self.ldapUser, self.ldapPass, userDN )

		try:
			out_bytes = subprocess.check_output( systemCommand, shell=True )
			out_text = out_bytes.decode('utf-8')
			
			modfile = open( '/tmp/selectedUser.ldiff', 'w+')
			modfile.truncate()
			modfile.write( '%s' % out_text )
			modfile.close()
			
			lines = out_text.split( "\n" )
			for line in lines:
				values = line.split( " " )
				if( len(values) > 1 ):
					attributeList[ values[0].replace(":", "") ] = values[1]    
			
			self.message = 'Get of users ok'

		except subprocess.CalledProcessError:
			self.message = 'Error command not exist: %s' % systemCommand
		except:
			self.message = 'Error: %s' % sys.exc_info()[0]

		return attributeList


# preparing statements
	def ldapAddPrepare( self, cn, basedn ):
		modfile = open( self.ldapTempModifyFile, 'w+')
		modfile.truncate()

		modfile.write( 'dn: uid=%s,%s\n' % ( cn, basedn ) )
		modfile.write( 'changetype: add\n' )
		modfile.write( 'objectClass: inetOrgPerson\n' )
		modfile.write( 'objectClass: posixAccount\n' )
		modfile.write( 'uid: %s\n' % cn )
		modfile.write( 'cn: %s\n' % cn )

		modfile.close()		

	def ldapChangePrepare( self, userDN ):
		modfile = open( self.ldapTempModifyFile, 'w+')
		modfile.truncate()

		modfile.write( 'dn: %s\n' % userDN )
		modfile.write( 'changetype: modify\n' )

		modfile.close()


# attribute manipulation
	def ldapAttributeChange( self, attribue, value, isInteger ):
		attribue = attribue.replace("\n", "")
		value = value.replace("\n", "")

		modfile = open( self.ldapTempModifyFile, 'a+')
		modfile.write( 'replace: %s\n' %  attribue )
		if isInteger == True:
			modfile.write( '%s: %s\n' % ( attribue, value ) )
		else:
			modfile.write( '%s: %s\n' % ( attribue, value ) )

		modfile.write( '-\n' )
		modfile.close()
        
	def ldapAttributeSet( self, attribue, value ):
		# check if value is not empty
		if len(value) < 1:
			return
		
		modfile = open( self.ldapTempModifyFile, 'a+')
		modfile.write( '%s: %s\n' % ( attribue, value ) )
		modfile.close()


# doing stuff
	def ldapChange( self ):
		systemCommand = 'ldapmodify -H %s -D "%s" -w "%s" -f %s >> %s 2>&1' % ( self.ldapHost, self.ldapUser, self.ldapPass, self.ldapTempModifyFile, self.ldapTempLogFile )

		modfile = open( self.ldapTempLogFile, 'w+')
		modfile.truncate()
		modfile.close()

		try:
			out_bytes = subprocess.check_output( systemCommand, shell=True )
			out_text = out_bytes.decode('utf-8')
			
			self.message = 'Get of users ok'

		except subprocess.CalledProcessError:
			self.message = 'Error command not exist: %s' % systemCommand
		except:
			self.message = 'Error: %s' % sys.exc_info()[0]

	def deleteDN( self, dn ):

		modfile = open( self.ldapTempModifyFile, 'w+')
		modfile.truncate()
		modfile.write( 'dn: %s\n' % dn )
		modfile.write( 'changetype: delete\n' )
		modfile.close()
		
		self.ldapChange()


# some special
	def logGet( self ):
		modfile = open( self.ldapTempLogFile, 'r')
		logData = modfile.read()
		modfile.close()
		
		return logData
       

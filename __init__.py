from ajenti.api import *
from ajenti.plugins import *


info = PluginInfo(
    title='LDAP-Simple',
    icon='group',
    dependencies=[
        PluginDependency('main'),
        BinaryDependency('ldapsearch'),
        BinaryDependency('ldapmodify'),
        BinaryDependency('slappasswd')
    ],
)


def init():
#	import api
    import main

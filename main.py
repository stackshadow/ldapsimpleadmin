import os
import subprocess

from ajenti.api import *
from ajenti.plugins.main.api import SectionPlugin
from ajenti.plugins.configurator.api import ClassConfigEditor
from ajenti.ui import on
from ajenti.ui.binder import Binder

from module.api import *


class ldapConfig (object):  # use new-style object at all times!
    def __init__(self):
        self.server = 'ldap://127.0.0.1:5000'
        self.basednuser = 'ou=users,dc=evilbrain,dc=de'
        self.basedngroup = 'ou=groups,dc=evilbrain,dc=de'
        self.adminuser = 'cn=superuser,dc=evilbrain,dc=de'
        self.adminpassword = ''
        self.userRequestPath = '/tmp/requestedUsers'

@plugin
class LDAPSimpleClassConfigEditor (ClassConfigEditor):
    title = 'LDAP-Simple Administrator'
    icon = 'table'

    def init(self):
        self.append(self.ui.inflate('ldapsimpleadmin:config')) # import file ldapsimple/config.xml


@plugin
class ldapsimple (SectionPlugin):
    default_classconfig = { 'server': 'localhost', 'basednuser': '', 'basedngroup': '', 'adminuser': '', 'adminpassword': '', 'requestpath':'' }
    classconfig_editor = LDAPSimpleClassConfigEditor
    ldapConnection = ldapAdmin
#    classconfig_root = True

    def init(self):
        self.title = _('LDAP-Simple Administrator')
        self.icon = 'group'
        self.category = _('System')

    # init the ui-interface
        self.append(self.ui.inflate('ldapsimpleadmin:main'))
#        self.find('connect').text = 'Connect to %s' % self.settings.server
        

        self.loadConfig()
        self.ldapConnection = ldapAdmin()
        self.ldapConnection.setCredentials( self.settings.server, self.settings.adminuser, self.settings.adminpassword )
        

    def loadConfig( self ):
        self.settings = ldapConfig()
        try:
			self.settings.server = self.classconfig['server']
			self.settings.basednuser = self.classconfig['basednuser']
			self.settings.basedngroup = self.classconfig['basedngroup']
			self.settings.adminuser = self.classconfig['adminuser']
			self.settings.adminpassword = self.classconfig['adminpassword']
			self.settings.userRequestPath = self.classconfig['requestpath']
        except:
			nothing = 1


    def posixUserClean( self ):
        self.find('posixUsercn').value = ''
        self.find('posixUsersn').value = ''
        self.find('posixUseruidNumber').value = ''
        self.find('posixUsergidNumber').value = ''
        self.find('posixUserhomeDirectory').value = ''
        self.find('posixUseruserPassword').value = ''
        self.find('posixUsermail').value = ''


    def posixUserLoadAttributes( self, ldapAttributes ):
		
		for attribute in ldapAttributes:

			# we dont load passwords !
			if attribute == "userPassword":
				continue

			# try to find the gui-object
			guiObject = self.find( 'posixUser%s' % attribute )
			try:
				guiObject.value = ldapAttributes[attribute]
			except:
				nothing = 1

    @on('config', 'click')
    def on_config_btn(self):
        self.context.launch('configure-plugin', plugin=self)

    @on('posixUserList', 'click')
    def on_posixUserList(self):

    # get user list
        userList = self.ldapConnection.posixUserList( self.settings.basednuser )
        self.find('posixUser').values = userList
        self.find('posixUser').labels = userList
        
        self.find('debug').text = self.ldapConnection.message
    
    # clean up interface
        self.posixUserClean()

    @on('posixUserLoad', 'click')
    def on_posixUserLoad(self):
        
        self.posixUserClean()

        selectedUser = self.find('posixUser').value
        selectedUserAttributes = self.ldapConnection.posixUserLoad( selectedUser )
        self.posixUserLoadAttributes( selectedUserAttributes )

    @on('posixUserNew', 'click')
    def on_posixUserNew(self):
        userNameNew = self.find('posixUsercn').value
        
        # we need a cool password
        newPassword = self.ldapConnection.encryptPassword( self.find('posixUseruserPassword').value )
        

        self.ldapConnection.ldapAddPrepare( userNameNew, self.settings.basednuser );
		
        self.ldapConnection.ldapAttributeSet( 'sn', self.find('posixUsersn').value )
        self.ldapConnection.ldapAttributeSet( 'uidNumber', self.find('posixUseruidNumber').value )
        self.ldapConnection.ldapAttributeSet( 'gidNumber', self.find('posixUsergidNumber').value )
        self.ldapConnection.ldapAttributeSet( 'homeDirectory', self.find('posixUserhomeDirectory').value )
        self.ldapConnection.ldapAttributeSet( 'userPassword', newPassword )
        self.ldapConnection.ldapAttributeSet( 'mail', self.find('posixUsermail').value )
        
        self.ldapConnection.ldapChange()
        self.find('debug').text = self.ldapConnection.logGet()

    @on('posixUserSave', 'click')
    def on_posixUserSave(self):
    # get selected user
        selectedUser = self.find('posixUser').value
        
	# we need a cool password
        newPassword = self.ldapConnection.encryptPassword( self.find('posixUseruserPassword').value )

        self.ldapConnection.ldapChangePrepare( selectedUser );
		
        self.ldapConnection.ldapAttributeChange( 'sn', self.find('posixUsersn').value, False )
        self.ldapConnection.ldapAttributeChange( 'uidNumber', self.find('posixUseruidNumber').value, True )
        self.ldapConnection.ldapAttributeChange( 'gidNumber', self.find('posixUsergidNumber').value, True )
        self.ldapConnection.ldapAttributeChange( 'homeDirectory', self.find('posixUserhomeDirectory').value, False )
        self.ldapConnection.ldapAttributeChange( 'userPassword', newPassword, False )
        self.ldapConnection.ldapAttributeChange( 'mail', self.find('posixUsermail').value, False )
        
        self.ldapConnection.ldapChange()
        self.find('debug').text = self.ldapConnection.logGet()

    @on('posixUserDelete', 'click')
    def on_posixUserDelete(self):
    # get selected user
        selectedUser = self.find('posixUser').value
        self.ldapConnection.deleteDN( selectedUser )
        self.find('debug').text = self.ldapConnection.logGet()
        

    #@on('groupOfNamesLoad', 'click')
    #def on_groupOfNamesLoad(self):
        
##        self.posixUserClean()

    
        #selectedGroupOfNames = self.find('groupOfNames').value
        #selectedGroupOfNamesAttributes = self.ldapConnection.getGroupOfNames( selectedGroupOfNames )

    ## load info of selected group
        #self.find('groupOfNamesCN').value = selectedGroupOfNamesAttributes['cn']
        #self.find('groupOfNamesMembers').values = selectedGroupOfNamesAttributes['member']
        #self.find('groupOfNamesMembers').labels = selectedGroupOfNamesAttributes['member']
        
    ## get user list
        #userList = self.ldapConnection.listPosixUser()
        #self.find('groupOfNamesUserlist').values = userList
        #self.find('groupOfNamesUserlist').labels = userList

    #@on('groupOfNamesUserRemove', 'click')
    #def on_groupOfNamesUserRemove(self):       
      
        ## get the selected user in member-group
        #members = self.find('groupOfNamesMembers').values
        #selectedMember = self.find('groupOfNamesMembers').value

        ## find it in the list and remove it
        #members.remove( selectedMember );
        
        #self.find('groupOfNamesMembers').values = members
        #self.find('groupOfNamesMembers').labels = members
      
    #@on('groupOfNamesUserAdd', 'click')
    #def on_groupOfNamesUserAdd(self):       
      
    ## get the selected user in member-group
        #selectedGroupOfNames = self.find('groupOfNames').value
        #selectedUser = self.find('groupOfNamesUserlist').value
        #members = self.find('groupOfNamesMembers').values
        
    ## append selected user to memberlist
        #members.append( selectedUser )

    ## find it in the list and remove it
        ##members.append( selectedMember );
        #if self.ldapConnection.addMemberToGroupOfNames( selectedGroupOfNames, members ) != True:
            #self.find('debug').text = '%s' % self.ldapConnection.errorMessage

    ## load again
        #self.on_groupOfNamesLoad()
      
    #@on('groupOfNamesNew', 'click')
    #def on_groupOfNamesNew(self):
        
        #name = str( self.find('groupOfNamesCN').value )
        #member = self.find('groupOfNamesMembers').values
        
        #if self.ldapConnection.addGroupOfNames( name, member ) != True:
            #self.find('debug').text = '%s' % self.ldapConnection.errorMessage

    #@on('groupOfNamesSave', 'click')
    #def on_groupOfNamesSave(self):
        
    ## get selected user
        #selectedGroupOfNames = self.find('groupOfNames').value

        #name = str( self.find('groupOfNamesCN').value )
        #member = repr( self.find('groupOfNamesMembers').values )

        #if self.ldapConnection.changeGroupOfNames( selectedGroupOfNames, name, member ) != True:
            #self.find('debug').text = '%s' % self.ldapConnection.errorMessage
        #else:
            #self.find('debug').text = 'Change %s: Ok..' % name
            
        

    #@on('groupOfNamesDelete', 'click')
    #def on_groupOfNamesDelete(self):
    ## get selected user
        #selectedGroupOfNames = self.find('groupOfNames').value
        
        #if self.ldapConnection.deleteDN( selectedGroupOfNames ) != True:
            #self.find('debug').text = 'Delete %s error: %s' % ( selectedGroupOfNames, self.ldapConnection.errorMessage )
        #else:
            #self.find('debug').text = 'Deleted..Ok'
        

      
      
      
      
      
      
      
      
      
        
        
        
        
        

